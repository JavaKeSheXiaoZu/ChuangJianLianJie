import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


public class DbTest {
	public static final String DBDRIVER="oracle.jdbc.driver.OracleDriver";
	public static final String DBURL="jdbc:oracle:thin:@localhost:1521:XSCJ";
	public static final String DBUSER="scott";
	public static final String DBPASS="tiger";
	public static void main(String[] args){
	     try{
	    	 //1.注册驱动
	    	 Class.forName(DBDRIVER);
	    	 //2.建立连接
	    	 Connection conn=DriverManager.getConnection(DBURL,DBUSER,DBPASS);
	    	 
	    	 System.out.println(conn);
	    	//3.创建Statement
	    	 Statement st=conn.createStatement();
	    	 //4.执行SQL语句
	          String sq1="select * from DEPT";
	    	  ResultSet rs=st.executeQuery(sq1);
	    	 //5.处理结果集
	    	 while(rs.next()){
	    		 Integer deptNo=rs.getInt("DEPTNO");
	    		 String dName=rs.getString("DNAME");
	    		 String loc=rs.getString("LOC");
	    		 System.out.println("DEPTNO:"+deptNo+"\tDNAME:"+dName+"\tLOC:"+loc);
                 
	    		 
	    		 
	    	 }
	    	 //6.关闭资源
	    	 rs.close();
	    	 st.close();
	    	 conn.close();
	    	 
	     }catch(Exception e){
	    	 e.printStackTrace();
	     }
	}

}
