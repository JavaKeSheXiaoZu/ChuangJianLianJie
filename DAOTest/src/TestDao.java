import java.sql.Connection;
import java.sql.DriverManager;


public class TestDao {
	public static final String DBDRIVER="oracle.jdbc.driver.OracleDriver";
	public static final String DBURL="jdbc:oracle:thin:@localhost:1521:XSCJ";
	public static final String DBUSER="scott";
	public static final String DBPASS="tiger";
	public static void main(String[] args) throws Exception {
		 //1.注册驱动
   	 Class.forName(DBDRIVER);
   	 //2.建立连接
   	 Connection conn=DriverManager.getConnection(DBURL,DBUSER,DBPASS);
   	 IDeptDAO deptDao=new IDeptDAOImpl(conn);
   	 Dept dept=new Dept();
   	 dept.setDeptNo(60);
   	 dept.setdName("GJSXY");
   	 dept.setLoc("JINCHUAN");
   	 if(deptDao.doCreate(dept)){
   		 System.out.println("添加成功");
   	 }
   	 Dept dept1= deptDao.findByDeptNo(60);
   	 System.out.println(dept1);   	 
	}

}
