import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class IDeptDAOImpl implements IDeptDAO{
	private Connection conn=null;
	public IDeptDAOImpl(Connection conn){
		this.conn=conn;
	}
	

	@Override
	public boolean doCreate(Dept dept) throws SQLException {
		boolean flag=false;
		PreparedStatement pstmt=null;
		String sq1="INSERT INTO DEPT(DEPTNO,DNAME,LOC)VALUES(?,?,?)";
		try{
			pstmt=conn.prepareStatement(sq1);
			pstmt.setInt(1, dept.getDeptNo());
			pstmt.setString(2, dept.getdName());
			pstmt.setString(3, dept.getLoc());
			int count=pstmt.executeUpdate();
			if(count>0){
				flag=true;
			}
		}catch(SQLException e){
			throw e;
		}finally{
			pstmt.close();
		}
		
		
		return flag;
	}

	@Override
	public boolean doUpdate(Dept dept) throws SQLException {
		boolean flag=false;
		PreparedStatement pstmt=null;
		String sq1="UPDATE DEPT SET DNAME=?,LOC=?   WHERE DEPTNO=?";
		try{
			pstmt=conn.prepareStatement(sq1);
			pstmt.setString(1, dept.getdName());
			pstmt.setString(2, dept.getLoc());
			pstmt.setInt(3, dept.getDeptNo());
			int count=pstmt.executeUpdate();
			if(count>0){
				flag=true;
			}
			
			
		}catch(SQLException e){
			throw e;
			
		}
		
		return false;
	}

	@Override
	public boolean deDelete(Integer deptNo) throws SQLException {
		boolean flag=false;
		PreparedStatement pstmt=null;
		String sq1="DELELT DEPT WHERE DEPTNO=?";
		try{
			pstmt=conn.prepareStatement(sq1);
			pstmt.setInt(1, deptNo);
			int count=pstmt.executeUpdate();
			if(count>0){
				flag=true;
			}
		}catch(SQLException e){
			throw e;
		}finally{
			pstmt.close();
		}
		return flag;
	}

	@Override
	public Dept findByDeptNo(Integer deptNo) throws SQLException {
		Dept dept=null;
		PreparedStatement pstmt=null;
		String sq1="SELECT * FROM DEPT WHERE DEPTNO=?";
		try{
			pstmt=conn.prepareStatement(sq1);
			pstmt.setInt(1, deptNo);
			ResultSet rs=pstmt.executeQuery();
			if(rs.next()){
				dept=new Dept();
				dept.setDeptNo(rs.getInt("DEPTNO"));
				dept.setdName(rs.getString("DNAME"));
				dept.setLoc(rs.getString("LOC"));
			}
		}catch(SQLException e){
			throw e;
		}finally{
			pstmt.close();
		}
		return dept;
	}

		

	@Override
	public List<Dept> findAll() throws SQLException {
		List<Dept> depts=null;
		PreparedStatement pstmt=null;
		String sq1="SELECT * FROM DEPT";
		try{
			pstmt=conn.prepareStatement(sq1);
		
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()){
				Dept dept=new Dept();
				dept.setDeptNo(rs.getInt("DEPTNO"));
				dept.setdName(rs.getString("DNAME"));
				dept.setLoc(rs.getString("LOC"));
				depts.add(dept);
				
			}
		}catch(SQLException e){
			throw e;
		}finally{
			pstmt.close();
		}
		return depts;
	}
	

}
