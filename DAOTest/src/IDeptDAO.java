
import java.sql.SQLException;
import java.util.List;


public interface IDeptDAO {
	public boolean doCreate(Dept dept)throws SQLException;
	public boolean doUpdate(Dept dept)throws SQLException;
	public boolean deDelete(Integer dept)throws SQLException;
	
	public Dept findByDeptNo(Integer deptNo) throws SQLException;
	public List<Dept>findAll() throws SQLException;
	
	
	
	
}
